
package edu.upc.damo;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText camp;
    TextView res;
    Button boto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      //  setContentView(R.layout.activity_constraint_layout);
        inicialitza();


    }

    private void inicialitza() {
        camp =  findViewById(R.id.camp);
        res =  findViewById(R.id.resultat);
        camp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });
        boto =  findViewById(R.id.botoEsborrar);


        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                esborraResultat(v);
            }
        }

        );

    }

    private void esborraResultat(View v) {
        res.setText("");
        camp.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)
        switch (actionId){
            case EditorInfo.IME_ACTION_GO:
                afegeixAResultat(camp.getText());
                camp.setText("");
                return true;
        }

        // Mirem quin event s'ha generat
        // (Cas de teclat hard)
        switch (event.getAction()) {
            case KeyEvent.ACTION_UP:
                afegeixAResultat(camp.getText());
                camp.setText("");
                return true;
        }
        return true;
    }

    private void afegeixAResultat(Editable text) {
        res.append("\n");
        res.append(text);
    }
}


